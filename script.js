class User {
  constructor(id, firstName, email) {
    this.id = id;
    this.firstName = firstName;
    this.email = email;
  }
}

class Post {
  constructor(id, userId, title, text) {
    this.id = id;
    this.userId = userId;
    this.title = title;
    this.text = text;
  }
}

class Card {
  constructor(post, user, onDelete) {
    this.post = post;
    this.user = user;
    this.onDelete = onDelete;
    this.element = this.createCardElement();
  }

  createCardElement() {
    const card = document.createElement('div');
    card.classList.add('card');

    const title = document.createElement('h2');
    title.textContent = this.post.title;

    const text = document.createElement('p');
    text.textContent = this.post.text;

    const userInfo = document.createElement('p');
    userInfo.textContent = `${this.user.firstName}, Email: (${this.user.email})`;

    const deleteButton = document.createElement('button');
    deleteButton.textContent = 'Delete';
    deleteButton.addEventListener('click', () => {
      this.onDelete(this.post.id);
      card.remove();
    });


    const editButton = document.createElement('button');
    editButton.textContent = 'Edit';
    editButton.addEventListener('click', () => this.editCard());

    card.appendChild(title);
    card.appendChild(text);
    card.appendChild(userInfo);
    card.appendChild(editButton);
    card.appendChild(deleteButton);

    return card;
  }

  editCard() {
    const newTitle = prompt('Enter new title:', this.post.title);
    const newText = prompt('Enter new text:', this.post.text);

    if (newTitle !== null && newText !== null) {
      const editedPost = {
        userId: this.post.userId,
        title: newTitle,
        body: newText
      };

      fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(editedPost)
      })
        .then(data => data.json())
        .then(updatedPost => {
          this.post.title = updatedPost.title;
          this.post.text = updatedPost.body;

          const titleElement = this.element.querySelector('h2');
          const textElement = this.element.querySelector('p');

          titleElement.textContent = updatedPost.title;
          textElement.textContent = updatedPost.body;
        })
        .catch(error => {
          console.error('Error editing post:', error);
        });
    }
  }

  getCardElement() {
    return this.element;
  }
}

function getUsers() {
  return fetch('https://ajax.test-danit.com/api/json/users')
    .then(data => data.json())
    .then(user => {
      return user.map(item => {
        return new User(item.id, item.name, item.email);
      })
    })
}

function getPosts() {
  return fetch('https://ajax.test-danit.com/api/json/posts')
    .then(data => data.json())
    .then(item => {
      return item.map(post => {
        return new Post(post.id, post.userId, post.title, post.body);
      })
    })
}

function deletePost(postId) {
  const isConfirm = confirm(`Are you sure you want to delete this post?`);
  if (!isConfirm) {
    return isConfirm;
  }
  fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
    method: 'DELETE'
  })
    .then(console.log(`Post with ID ${postId} deleted successfully.`))
    .catch(err => {
      console.error(`${err}. Failed to delete post with ID ${postId}.`);
    })
}

document.getElementById('postBtn').addEventListener('click', createPost);

function createPost() {
  const title = prompt('Enter the title of the post:');
  const text = prompt('Enter the text of the post:');
  const name = prompt('Enter your name: ');
  const email = prompt('Enter your email: ')

  const newPost = {
    userId: 1,
    title: title,
    body: text
  };

  fetch('https://ajax.test-danit.com/api/json/posts', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(newPost)
  })
    .then(data => data.json())
    .then(createdPost => {
      const post = new Post(1, createdPost.userId, createdPost.title, createdPost.body);
      const user = new User(1, name, email);

      const onDelete = postId => {
        deletePost(postId);
      };

      const card = new Card(post, user, onDelete);
      const cardsContainer = document.getElementById('cards__container');

      cardsContainer.prepend(card.getCardElement());
    })
    .catch(error => {
      console.error('Error creating post:', error);
    });
}

function initPosts() {
  Promise.all([getUsers(), getPosts()])
    .then(([users, posts]) => {
      const cardsContainer = document.getElementById('cards__container');

      posts.forEach(post => {
        const user = users.find(item => item.id === post.userId);
        const onDelete = postId => {
          deletePost(postId);
        };

        const card = new Card(post, user, onDelete);
        cardsContainer.appendChild(card.getCardElement());
      });
    });
}

initPosts();
